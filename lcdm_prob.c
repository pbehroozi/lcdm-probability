#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>
#include "check_syscalls.h"
#include "distance.h"
#include "mf_cache.h"

struct survey {
  double area, z1, z2, error, prob, equiv_outlier, nd;
  int64_t type;
  char name[1024];
};

struct survey *surveys = NULL;
int64_t num_surveys = 0;

double lognormal(double dm) {
  return M_SQRT1_2*exp(-0.5*dm*dm);
}

double cauchy(double dm) {
  return (1.0/(M_PI*((dm*dm)+1.0)));
}

int main(int argc, char **argv) {
  char buffer[1024];
  double total_prob=1;
  
  load_mf_cache("mf_planck.dat", 0);
  
  if (argc < 3) {
    fprintf(stderr, "Usage: %s surveys.txt outlier_sigma\n", argv[0]);
    fprintf(stderr, "surveys.txt format:\n");
    fprintf(stderr, " name(no_spaces) area/deg^2 redshift_min redshift_max error [error_type]\n");
    fprintf(stderr, "  error types: 0 (log10-normal, default); 1 (cauchy)\n");
    exit(EXIT_FAILURE);
  }

  double sigma = atof(argv[2]);
  FILE *in = check_fopen(argv[1], "r");
  struct survey s = {0};
  while (fgets(buffer, 1024, in)) {
    s.type = 0;
    if (sscanf(buffer, "%s %lf %lf %lf %lf %"SCNd64,
	       s.name, &s.area, &s.z1, &s.z2, &s.error, &s.type) < 5)
      continue;
    check_realloc_every(surveys, sizeof(struct survey), num_surveys, 10);
    if (s.z2 < s.z1) {
      double z = s.z1;
      s.z1 = s.z2;
      s.z2 = z;
    }
    surveys[num_surveys] = s;
    num_surveys++;
  }
  fclose(in);

  
  //Convert sigma into a fraction of realizations
  double f = 0.5*erfc((sigma)/M_SQRT2);
  fprintf(stderr, "Expected number of %g-sigma outliers per survey: %e\n", sigma, f);

    
  int64_t i, j, k;
  double z, m, dz;
#define M_MIN 8
#define M_MAX 18
#define BPDEX 200  
#define NUM_BINS ((M_MAX-M_MIN)*BPDEX)
  double mf[NUM_BINS] = {0}, cmf[NUM_BINS], ccmf[NUM_BINS], weights[NUM_BINS*2+1];
  for (i=0; i<num_surveys; i++) {
    memset(mf, 0, sizeof(double)*NUM_BINS);
    memset(cmf, 0, sizeof(double)*NUM_BINS);
    memset(ccmf, 0, sizeof(double)*NUM_BINS);
    
    //Integrate mass function for survey
    dz = (s.z2-s.z1)/100.0;
    double area_factor = surveys[i].area / (4.0*180.0*180.0/M_PI);
    for (z=s.z1+dz/2.0; z<s.z2; z+=dz) {
      double v = area_factor*(comoving_volume(z+dz/2.0)-comoving_volume(z-dz/2.0));
      double a = 1.0/(1.0+z);
      for (j=0; j<NUM_BINS; j++) {
	m = M_MIN + (double)(j+0.5)/(double)BPDEX;
	mf[j] += v*pow(10, mf_cache(a, m));
      }
    }

    //Convert to cumulative MF
    surveys[i].equiv_outlier = 0;
    for (j=NUM_BINS-2; j>=0; j--) {
      cmf[j] = cmf[j+1]+mf[j]/(double)BPDEX;
      if (!surveys[i].equiv_outlier && cmf[j] > f) {
	surveys[i].equiv_outlier = M_MIN+(double)j/(double)BPDEX + (f-cmf[j])*(-1.0/(double)BPDEX)/(cmf[j]-cmf[j+1]);
	//fprintf(stderr, "Equiv outlier: %f; %g @ %g; %g @ %g; %g\n", surveys[i].equiv_outlier,  M_MIN+(double)j/(double)BPDEX , cmf[j],  M_MIN+(double)(j+1)/(double)BPDEX , cmf[j+1], f);
      }
    }

    
    //Convolve MF with errors
    if (!surveys[i].error) {
      //fprintf(stderr, "[Warning] Survey %"PRId64" (%s) has zero mass error!\n", i, surveys[i].name);
      memcpy(ccmf, mf, sizeof(double)*NUM_BINS);
    } else {
      //Create weights array
      for (j=0; j<NUM_BINS*2+1; j++) {
	double dm = (double)(j-NUM_BINS)/(surveys[i].error*(double)BPDEX);
	weights[j] = (surveys[i].type == 1) ? cauchy(dm) : lognormal(dm);
	if (j>0) weights[j]+=weights[j-1];
      }
      
      for (j=0; j<NUM_BINS; j++) { //Original MF
	int64_t djmin = -j + NUM_BINS;
	int64_t djmax = NUM_BINS - j + NUM_BINS;
	double weight = weights[djmax];
	if (djmin > 0) weight -= weights[djmin-1];
	if (weight > 0) weight = 1.0/weight;
	//fprintf(stderr, "%g\n", weight);
	for (k=0; k<NUM_BINS; k++) { //Convolved MF
	  double dm = (double)(j-k)/(surveys[i].error*(double)BPDEX);
	  if (surveys[i].type==1)
	    ccmf[k]+=weight*cauchy(dm)*mf[j];
	  else
	    ccmf[k]+=weight*lognormal(dm)*mf[j];
	}
      }
    }

    //Convert convolved MF to cumulative MF
    ccmf[NUM_BINS-1]=0;
    for (j=NUM_BINS-2; j>=0; j--) {
      ccmf[j] = ccmf[j+1]+ccmf[j]/(double)BPDEX;
      double m = M_MIN+(double)j/(double)BPDEX;
      if (m>=surveys[i].equiv_outlier-1.001/(double)BPDEX && m < surveys[i].equiv_outlier) {
	surveys[i].nd = ccmf[j] + (surveys[i].equiv_outlier-m)*(-(double)BPDEX)*(ccmf[j]-ccmf[j+1]);
	//fprintf(stderr, "Equiv ND: %g; %g @ %g; %g @ %g; %g\n\n", surveys[i].nd,  M_MIN+(double)j/(double)BPDEX , ccmf[j],  M_MIN+(double)(j+1)/(double)BPDEX , ccmf[j+1], surveys[i].equiv_outlier);
      }
    }
    surveys[i].prob = 1.0-exp(-surveys[i].nd);

    printf("Survey %"PRId64": %s\n", i, surveys[i].name);
    printf("Area: %g deg^2 (z=%g-%g)\n", surveys[i].area, surveys[i].z1, surveys[i].z2);
    printf("Equivalent (%g-sigma) halo mass outlier: 10^%g Msun\n", sigma, surveys[i].equiv_outlier);
    printf("Error distribution: width: %f; type: %s\n", surveys[i].error, ((surveys[i].type==1) ? "cauchy" : "log10-normal"));
    printf("Probability of detecting %g-sigma outlier given error distribution: %g\n", sigma, surveys[i].prob);
    printf("\n");
    total_prob *= 1.0-surveys[i].prob;

    snprintf(buffer, 1024, "survey_%"PRId64".mf.txt", i);
    FILE *out = check_fopen(buffer, "w");
    fprintf(out, "#Survey %"PRId64": %s\n", i, surveys[i].name);
    fprintf(out, "#Area: %g deg^2 (z=%g-%g)\n", surveys[i].area, surveys[i].z1, surveys[i].z2);
    fprintf(out, "#Equivalent (%g-sigma) halo mass outlier: 10^%g Msun\n", sigma, surveys[i].equiv_outlier);
    fprintf(out, "#Error distribution: width: %f; type: %s\n", surveys[i].error, ((surveys[i].type==1) ? "cauchy" : "log10-normal"));
    fprintf(out, "#Probability of detecting %g-sigma outlier given error distribution: %g\n", sigma, surveys[i].prob);
    fprintf(out, "#Mass(Msun) Cumulative_ND(counts/survey) Cumulative_ND_With_Errors(counts/survey)\n");
    for (j=0; j<NUM_BINS; j++) {
      fprintf(out, "%g %g %g\n", M_MIN+(double)j/(double)BPDEX, cmf[j], ccmf[j]);
    }
    fclose(out);
  }

  printf("Total combined probability of %g-sigma outlier: %g\n", sigma, 1.0-total_prob);
  return 0;
}
