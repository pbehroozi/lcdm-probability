CFLAGS=-Wall -fno-math-errno -fPIC -D_GNU_SOURCE
LDFLAGS=-shared
OFLAGS = -lm -O3 -std=c99
DEBUGFLAGS = -lm -g -O0 -std=c99 -Dinline= 
PROFFLAGS = -lm -g -pg -O2 -std=c99
CC = gcc
DIST_FLAGS =

all:
	@make reg EXTRA_FLAGS="$(OFLAGS)"

debug:
	@make reg EXTRA_FLAGS="$(DEBUGFLAGS)"

prof:
	@make reg EXTRA_FLAGS="$(PROFFLAGS)"

reg:
	$(CC) $(CFLAGS) check_syscalls.c distance.c lcdm_prob.c mf_cache.c universe_time.c -o lcdm_prob  $(EXTRA_FLAGS)
#	$(CC) $(CFLAGS) $(CFILES_OLD) -o nd_redshift_old  $(EXTRA_FLAGS)

dist:
	cd ../ ;  tar -czvf lcdm_prob.tar.gz lcdm-probability/*.[ch] lcdm-probability/Makefile lcdm-probability/mf_planck.dat; mv lcdm_prob.tar.gz lcdm-probability

clean:
	rm -f *~ lcdm_prob
